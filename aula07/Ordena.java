public class Ordena {
    
    public static void quickSort(int v[]) {
        quickSortRec(v, 0, v.length-1);
    }

    private static void quickSortRec(int v[], int lo, int hi) {
        int e = lo;
        int d= hi;

        int pivo = v[(e + d) / 2]; // o pivo será o elemento central do vetor

        while (e <= d) {  // enquanto as posições não cruzarem indicando que percorremos todo o vetor
            while (v[e] < pivo) { // enquanto o valor à esquera continuar menor que o pivo
                e++;
            }
            while (v[d] > pivo) { // enquanto o valor à direita continuar maior que o pivo
                d--;
            }
            if(e <= d) { // troca os valores se ainda não terminou o vetor e foram encontrados valores fora do lugar
                int aux = v[e];
                v[e] = v[d];
                v[d] = aux;
                e++;
                d--;
            }
        }
        if(d - lo > 0) { // verifica se "sobraram" números no vetor à esquerda do pivo
            quickSortRec(v, lo, d);
        }
        if(hi - e > 0) { // verifica se "sobraram" números no vetor à direita do pivo
            quickSortRec(v, e, hi);
        }
    }
}
