/**
 * Main
 */
public class Main {

    public static void main(String[] args) {
        final int TAM = 20;
        int vet[] = new int[TAM];

        System.out.println("Antes:");
        for (int i = 0; i < vet.length; i++) {
            vet[i] = (int)(Math.random() * TAM);
            System.out.print(vet[i] + " ");
        }

        Ordena.quickSort(vet);

        System.out.println("\nDepois:");
        for (int i = 0; i < vet.length; i++) {
            System.out.print(vet[i] + " ");
        }
    }
}