package exemplo01;


public class Exemplo01 {
    

    public static void main(String[] args) {
        int numero = 5;
        Pessoa p; // p é uma referência para um objeto do tipo Pessoa

        System.out.println(numero);

        numero = 8;

        System.out.println(numero);

        p = new Pessoa(); // criou um objeto, e atribui a referência deste objeto para o 'p'
        p.idade = 12;

        System.out.println(p.idade);
        System.out.println(p.nome);

        p = new Pessoa(); // criou um novo objeto, e atribui a referência deste objeto para o 'p'

        System.out.println(p.idade);
    }
    
}