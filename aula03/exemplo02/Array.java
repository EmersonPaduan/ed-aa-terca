package exemplo02;

import java.util.ArrayList;

public class Array {
    public static void main(String[] args) {
        ArrayList<Integer> lista = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            lista.add(i);
        }

        // System.out.println(lista/* .toString() */);
        for (int i = 0; i < lista.size(); i++) {
            System.out.print(lista.get(i) + " ");
        }
        
        System.out.println("\n----");
        for (Integer numero : lista) { // para cada número da lista faça
            System.out.print(numero + " ");
        }

        System.out.println("\nremovendo");
        lista.remove(1);
        System.out.println(lista);
        System.out.println(lista.get(1));

    }
}
