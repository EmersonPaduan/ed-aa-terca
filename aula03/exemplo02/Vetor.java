package exemplo02;

public class Vetor {
    public static void main(String[] args) {
        int vet[] = new int[5];

        for (int i = 0; i < vet.length; i++) {
            vet[ i ] = i;
        }

        // System.out.println(vet);
        for (int i = 0; i < vet.length; i++) {
            System.out.print(vet[ i ] + " ");
        }
    }
}
