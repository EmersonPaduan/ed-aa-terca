package exercicio;

import java.util.Scanner;

public class Exercicio01 {
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        int numero;

        System.out.print("Informe o valor do N: ");
        numero = teclado.nextInt(); 
    
        System.out.println("Soma de 1 até " + numero + " = " + soma(numero));

        teclado.close();
    }

    static int soma(int n) {
        int soma = 0;

        for (int i = 1; i <= n; i++) {
            soma += i;
        }

        return soma;
    }
}
