package exercicio;

import java.util.Scanner;

public class exercicio02 {
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        int numero;

        System.out.print("Informe o valor do N: ");
        numero = teclado.nextInt(); 
    
        System.out.println("Soma de 1 até " + numero + " = " + soma(numero));

        teclado.close();
    }

    // 1 ... N
    // n = 1, -> 1 caso base
    // 1..N ->  N + (1 .. N-1)) -> N + (N-1) + (1.. N-2)
    static int soma(int n) {
        if(n <= 1) { // caso base
            return n;
        }
        return n + soma(n-1);
    }
}
