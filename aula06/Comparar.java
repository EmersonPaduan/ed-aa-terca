public class Comparar {
    public static void main(String[] args) {
        final int TAM_MAX = 1_000_000_000;
        long inicio, termino;

        int vet[] = new int[TAM_MAX];

        for (int i = 0; i < TAM_MAX; i++) {
            vet[i] = i;
        }

        int procurado = (int) (Math.random() * TAM_MAX);

        inicio = System.currentTimeMillis();
        int resp1 = Busca.buscaSequencial(vet, procurado);
        termino = System.currentTimeMillis();
        System.out.println("Sequencial: " + resp1);
        System.out.println("Levou " + (termino - inicio));
        
        inicio = System.currentTimeMillis();
        int resp2 = Busca.buscaBinaria(vet, procurado);
        termino = System.currentTimeMillis();
        System.out.println("Binário: " + resp2);
        System.out.println("Levou " + (termino - inicio));


    }
}
