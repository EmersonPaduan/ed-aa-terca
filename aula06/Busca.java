
public class Busca {

    public static int buscaSequencial(int v[], int key) {
        for (int i = 0; i < v.length; i++) {
            if(key == v[i]) {
                return i;
            }
        }
        return -1;
    }

    public static int buscaSequencialRec(int v[], int key, int tam) {
        if(tam == 0) {
            return -1;
        }
        if(key == v[tam-1]) {
            return tam-1;
        }
        return buscaSequencialRec(v, key, tam-1);
    }

    public static int buscaBinaria(int v[], int key) {
        int inicio = 0;
        int fim = v.length-1;
        int meio;

        while(inicio <= fim) {
            meio = (inicio + fim) / 2;
            if(key == v[meio]) {
                return meio;
            }
            if(key < v[meio]) {
                fim = meio -1;
            } else {
                inicio = meio + 1;
            }
        }
        return -1;
    }
    
}