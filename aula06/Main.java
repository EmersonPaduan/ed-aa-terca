public class Main {
    public static void main(String[] args) {
        final int TAM_MAX = 10;

        int vet[] = new int[TAM_MAX];

        for (int i = 0; i < TAM_MAX; i++) {
            // vet[i] = (int) (Math.random() * TAM_MAX);
            vet[i] = i;
            System.out.print(vet[i] + " ");
        }

        int indice = Busca.buscaBinaria(vet, 5);
        if(indice == -1) {
            System.out.println("\nNão encontrado");
        } else {
            System.out.println("\nEncontrado no índice " + indice);
        }
    }
}
