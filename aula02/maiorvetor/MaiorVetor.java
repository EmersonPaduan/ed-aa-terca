package maiorvetor;

public class MaiorVetor {
    public static void main(String[] args) {
        final int TAM = 10;
        int vetor[] = new int[10];

        for (int i = 0; i < TAM; i++) {
            vetor[i] = (int) (Math.random() * TAM * 10);
        }

        System.out.println(vetor);
        for (int i = 0; i < TAM; i++) {
            System.out.print(vetor[i] + " ");
        }
        System.out.println("\nMaior: " + maior(vetor, TAM));
    }

    static int maior(int vetor[], int tam) {
        if(tam == 1) {
            return vetor[0];
        }
        int maiorParcial = maior(vetor, tam-1);

        if(vetor[tam-1] > maiorParcial){
            return vetor[tam-1];
        }
        return maiorParcial;
    }
}
