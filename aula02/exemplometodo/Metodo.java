package exemplometodo;

public class Metodo {
    public static void main(String[] args) {
        metodo1();
        System.out.println("Final");
    }

    public static void metodo1() {
        System.out.println("inicio do metodo 1");
        metodo2();
        System.out.println("final do metodo 1");
    }

    public static void metodo2() {
        System.out.println("inicio do metodo 2");
        metodo3();
        System.out.println("final do metodo 2");
    }

    public static void metodo3() {
        System.out.println("inicio do metodo 3");
    }
}
