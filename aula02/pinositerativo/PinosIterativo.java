package pinositerativo;

public class PinosIterativo {

    public static void main(String[] args) {
        int linhas = 4;

        System.out.println("Para " + linhas + " linhas  o total é " + pinositerativo(linhas));

    }

    static int pinositerativo(int numeroLinhas) {
        int totalPinos = 0;

        for (int linha = 1; linha <= numeroLinhas; linha++) {
            totalPinos += linha;
        }

        return totalPinos;
    }
}