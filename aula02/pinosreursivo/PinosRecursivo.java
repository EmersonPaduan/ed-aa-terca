package pinosreursivo;

public class PinosRecursivo {
    public static void main(String[] args) {
        int linhas = 3;

        int resposta = pinosReursivo(linhas);
        
        System.out.println("Para " + linhas + " linhas  o total é " + resposta);

    }

    static int pinosReursivo(int numeroLinhas) {
        // caso base - ponto de parada
        if(numeroLinhas == 1) {
            return 1;
        }
        // caso geral
        return numeroLinhas + pinosReursivo(numeroLinhas - 1);
    }
}
