package fatorialrecursivo;

public class FatorialRecursivo {
    public static void main(String[] args) {
        int n = 5;

        System.out.println("O fatorial de " + n + " é "  + fatorialRecursivo(n));
    }

    static int fatorialRecursivo(int numero) {
        if(numero == 0) {
            return 1;
        }

        return numero * fatorialRecursivo(numero - 1);
    }
}
