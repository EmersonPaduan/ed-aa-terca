package fatorialiterativo;

public class FatorialIterativo {
    public static void main(String[] args) {
        int n = 5;

        System.out.println("O fatorial de " + n + " é "  + fatorialIterativo(n));
    }

    static int fatorialIterativo(int numero) {
        int fat = 1;
        
        for (int i = numero; i  > 0; i--) {
            fat *= i; // fat = fat * i;
        }

        return fat;
    }
}
