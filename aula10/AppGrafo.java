public class AppGrafo {
    public static void main(String[] args) {
        Grafo grafo = new Grafo();

        grafo.novoVertice("v1");
        grafo.novoVertice("v2");
        grafo.novoVertice("v3");
        grafo.novoVertice("v4");

        System.out.println("inserindo aresta: " + grafo.novaAreasta("v1", "v2"));
        System.out.println("inserindo aresta: " + grafo.novaAreasta("v1", "v3"));
        System.out.println("inserindo aresta: " + grafo.novaAreasta("v2", "v1"));
        System.out.println("inserindo aresta: " + grafo.novaAreasta("v3", "v4"));

        System.out.println(grafo);
        System.out.println("Areastas do grafo: " + grafo.arestas());

        System.out.println("Arestas do vertice v1: " + grafo.arestas("v1"));

    }
}
