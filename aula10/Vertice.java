import java.util.ArrayList;

public class Vertice {
    private String nome;
    private ArrayList<String> adjascentes;

    public Vertice(String nome) {
        this.nome = nome;
        adjascentes = new ArrayList<>();
    }

    public String getNome() {
        return nome;
    }
    
    public void adicionaAdjascente(String nome) {
        adjascentes.add(nome);
    }

    public String arestas() {
        String saida = "";
        for (String vertice : adjascentes) {
            saida += "(" + nome + "," + vertice + ") ";
        }
        return saida;
    }

    @Override
    public String toString() {
        String saida = nome + ": ";
        for (String vertice : adjascentes) {
            saida += vertice + " / ";
        }
        return saida;
    }
}