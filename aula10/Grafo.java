import java.util.HashMap;

public class Grafo {
    private HashMap<String, Vertice> vertices;

    public Grafo() {
        vertices = new HashMap<>();
    }

    public void novoVertice(String nome) {
        vertices.put(nome, new Vertice(nome));
    }

    public boolean novaAreasta(String verticeOrigem, String verticeDestino) {
        if(vertices.containsKey(verticeOrigem) && vertices.containsKey(verticeDestino)) {
            vertices.get(verticeOrigem).adicionaAdjascente(verticeDestino);
            return true;
        }
        return false;
    }

    public String arestas() {
        String saida = "[ ";
        for (Vertice vertice: vertices.values()) {
            saida += vertice.arestas();
         }
         saida += " ]";
         return saida;
    }

    public String arestas(String nome) {
         return "[ " +  vertices.get(nome).arestas() + "]";
    }

    @Override
    public String toString() {
        String saida = "";

        for (Vertice vertice: vertices.values()) {
            saida += vertice + "\n";
        }

        return saida;
    }
}
