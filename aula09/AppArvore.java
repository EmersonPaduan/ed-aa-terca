public class AppArvore {
    public static void main(String[] args) {
        ArvoreBB arvore = new ArvoreBB();

        System.out.println("Arvore vazia? " + arvore.estaVazia());

        arvore.inserir(5);

        System.out.println("Arvore vazia? " + arvore.estaVazia());

        arvore.inserir(2);
        arvore.inserir(15);
        arvore.inserir(8);
        arvore.inserir(8);
        arvore.inserir(4);
        arvore.inserir(7);

        System.out.println("Em ordem");
        arvore.emOrdem();
        System.out.println("\nPre ordem");
        arvore.preOrdem();
        System.out.println("\nPos ordem");
        arvore.posOrdem();

        System.out.println("\nProcurando o valor 8: " + ((arvore.busca(8)) ? "achou" : "não achou"));
        System.out.println("Procurando o valor 20: " + ((arvore.busca(20)) ? "achou" : "não achou"));

    }
}
