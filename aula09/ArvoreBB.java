public class ArvoreBB {
    private No raiz;

    public ArvoreBB() {
        raiz = null;
    }

    public boolean estaVazia() {
        return (raiz == null);
    }

    public boolean inserir(int novoValor) {
        No novoNo = new No(novoValor);

        if (estaVazia()) {
            raiz = novoNo;
            return true;
        }

        No aux = raiz;
        No anterior;

        do {
            anterior = aux; // guarda a referencia para o No atual

            if (novoValor == aux.getDado()) {
                return false; // valores duplicados não podem ser inseridos
            }

            if (novoValor < aux.getDado()) {
                aux = aux.getEsquerda();
                if (aux == null) { // chegou em uma posição livre
                    anterior.setEsquerda(novoNo); // insere o novoNo a esquerda do No anterior
                }
            } else {
                aux = aux.getDireita();
                if (aux == null) {
                    anterior.setDireita(novoNo);
                }
            }
        } while (aux != null);
        return true;
    }

    public void emOrdem() {
        emOrdem(raiz);
    }

    private void emOrdem(No raiz) {
        if(raiz == null) {
            return;
        }
        emOrdem(raiz.getEsquerda());
        System.out.print(raiz.getDado() + " ");
        emOrdem(raiz.getDireita());
    }
    
    public void preOrdem() {
        preOrdem(raiz);
    }

    private void preOrdem(No raiz) {
        if(raiz == null) {
            return;
        }
        System.out.print(raiz.getDado() + " ");
        preOrdem(raiz.getEsquerda());
        preOrdem(raiz.getDireita());
    }

    public void posOrdem() {
        posOrdem(raiz);
    }

    private void posOrdem(No raiz) {
        if(raiz == null) {
            return;
        }
        posOrdem(raiz.getEsquerda());
        posOrdem(raiz.getDireita());
        System.out.print(raiz.getDado() + " ");
    }

    public boolean busca(int valorProcurado) {
        if(estaVazia()){
            return false;
        }

        No aux = raiz;

        while (aux != null) {
            if(valorProcurado == aux.getDado()) {
                return true;
            }
            if(valorProcurado < aux.getDado()) {
                aux = aux.getEsquerda();
            } else {
                aux = aux.getDireita();
            }
        }

        return false; // Não achou o valor na árvore
    }
}
