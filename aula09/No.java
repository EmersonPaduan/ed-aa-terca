
public class No {
    private int dado;
    private No esquerda;
    private No direita;
    
    public No (int dado) {
        this.dado = dado;
        esquerda = null;
        direita = null;
    }

    public int getDado() {
        return dado;
    }

    public No getEsquerda() {
        return esquerda;
    }

    public void setEsquerda(No esquerda) {
        this.esquerda = esquerda;
    }

    public No getDireita() {
        return direita;
    }

    public void setDireita(No direita) {
        this.direita = direita;
    }

}
