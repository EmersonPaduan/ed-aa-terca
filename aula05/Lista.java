public class Lista {
    private No inicio;
    private No ultimo;

    public Lista() {
        inicio = ultimo = null;
    }

    public boolean estaVazia() {
        return (inicio == null);
    }

    public void inserirFinal(String dado) {
        No novoNo = new No(dado);
        if (estaVazia()) {
            inicio = novoNo;
        } else {
            ultimo.setProximo(novoNo);
        }
        ultimo = novoNo;
    }

    public void inserirInicio(String dado) {
        No novoNo = new No(dado);
        if (estaVazia()) {
            inicio = novoNo;
            ultimo = novoNo;
            return;
        }
        novoNo.setProximo(inicio); // ajusta o novoNo como sendo o primeiro da lista
        inicio = novoNo;
    }

    public void inserir(String dado, int posicao) {
        if(posicao <= 0) {
            return;
        }
        if(posicao == 1 || estaVazia()) {
            inserirInicio(dado);
            return;
        }
        No aux = inicio;

        while (posicao > 2 && aux != null) {
            aux = aux.getProximo();
            posicao--;
        }

        if(aux == null) {
            inserirFinal(dado);
            return;
        }

        No novoNo = new No(dado);
        novoNo.setProximo(aux.getProximo());
        aux.setProximo(novoNo);
        if(aux == ultimo) {
            ultimo = novoNo;
        }
    }

    public String removerInicio() {
        if(estaVazia()) {
            return null;
        }

        String dadoRemovido = inicio.getDado();
        inicio = inicio.getProximo(); // move a referência do início para o próximo No
        if(inicio == null) {
            ultimo = null;
        }

        return dadoRemovido;
    }

    public String removerFinal() {
        if(estaVazia()) {
            return null;
        }

        String dadoRemovido = ultimo.getDado();
        if(inicio == ultimo) {
            inicio = ultimo = null;
            return dadoRemovido;
        }

        No aux = inicio;
        while (aux.getProximo() != ultimo) {
            aux = aux.getProximo();
        }

        ultimo = aux;
        ultimo.setProximo(null);

        return dadoRemovido;
    }


    public String remover(int posicao) {
        if(estaVazia() ||  posicao <= 0) {
            return null;
        }
        if(posicao == 1) {
            return removerInicio();
        }

        No aux = inicio;
        int cont = 1;
        while ( cont < posicao -1 && aux != null) { // move o aux para uma posição anterior ao NO que queremos remover
            aux = aux.getProximo();
            cont++;
        }
        if(aux == null || aux == ultimo ) { // a posição informada no parâmetro não é válida
            return null;
        }
        if(aux.getProximo() == ultimo) {
            return removerFinal();
        }
        String dadoRemovido = aux.getProximo().getDado();

        aux.setProximo(aux.getProximo().getProximo());

        return dadoRemovido;
    }

    public void exibir() {
        if (estaVazia()) {
            System.out.println("Lista Vazia");
            return;
        }
        No aux = inicio;
        do {
            System.out.print(aux.getDado() + " ");
            aux = aux.getProximo(); // move o No auxiliar para o próximo No da Fila
        } while (aux != null);
    }
}
