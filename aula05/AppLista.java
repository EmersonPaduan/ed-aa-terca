public class AppLista {
    public static void main(String[] args) {
        Lista lista = new Lista();

        // lista.inserirInicio("A");
        // lista.inserirFinal("B");
        // lista.inserirFinal("C");
        // lista.inserirInicio("X");

        System.out.println("Removendo o ultimo elemento:");
        System.out.println("Revido: " + lista.remover(1));

        System.out.println("Removendo o ultimo elemento:");
        System.out.println("Revido: " + lista.remover(0));

        lista.inserir("A", 1);

        System.out.println("Removendo o ultimo elemento:");
        System.out.println("Revido: " + lista.remover(1));

        lista.inserir("B", 1);
        lista.inserir("C", 2);
        lista.inserir("D", 3);


        System.out.println("Removendo o ultimo elemento:");
        System.out.println("Revido: " + lista.remover(3));

        System.out.println("Final:");
        lista.exibir();
    }
}
