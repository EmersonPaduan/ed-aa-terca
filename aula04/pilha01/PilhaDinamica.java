package pilha01;

import java.util.ArrayList;

public class PilhaDinamica {
    private ArrayList<String> conteudo;
    private final int MAX;

    public PilhaDinamica() {
        conteudo = new ArrayList<>();
        MAX = -1;
    }

    public PilhaDinamica(int max) {
        conteudo = new ArrayList<>();
        MAX = max;
    }

    public boolean empilhar(String dado) {
        if(estaCheia()) {
            return false;
        }
        conteudo.add(dado); // insere no final do ArrayList (topo da pilha)
        return true;
    }

    public String desempilhar() {
        if(!estaVazia()) {
            return conteudo.remove(conteudo.size() - 1); // remover do final (topo)
        }
        return null;
    }
    
    public boolean estaVazia() {
        return conteudo.isEmpty();
    }

    public boolean estaCheia() {
        if(MAX < 0) {
            return false;
        }
        return (conteudo.size() == MAX);
    }

 }
