package pilha01;

public class AppPilha01 {
    public static void main(String[] args) {
        PilhaDinamica p = new PilhaDinamica(3);

        for (int i = 0; i < 10; i++) {
            p.empilhar(String.valueOf(i));
        }

        while (!p.estaVazia()) {
            System.out.print(p.desempilhar() + " ");
        }
    }
}
