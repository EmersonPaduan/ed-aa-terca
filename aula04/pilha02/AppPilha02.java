package pilha02;

public class AppPilha02 {
    public static void main(String[] args) {
        PilhaDinamica p = new PilhaDinamica();

        System.out.println("Pilha Vazia? " + p.estaVazia());
        p.empilhar("Daniela");
        p.empilhar("Roberto");
        p.empilhar("David");

        p.exibirRecursivo();

        while(!p.estaVazia()) {
            System.out.println("Desempilhando: " + p.desempilhar());
        }

        System.out.println("Nova pilha:");
        p.exibir();
    }
}
