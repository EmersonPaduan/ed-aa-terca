package pilha02;

public class No {
    private String dado;
    private No proximo;

    public No(String dado) {
        this.dado = dado;
        // proximo = null;
    }

    public String getDado() {
        return dado;
    }

    public No getProximo() {
        return proximo;
    }

    public void setProximo(No proximo) {
        this.proximo = proximo;
    }
}
