package pilha02;

public class PilhaDinamica {
    private No inicio, topo;
    private final int MAX;
    private int qtdeItens;

    public PilhaDinamica() {
        inicio = null;
        topo = null;
        qtdeItens = 0;
        MAX = -1;
    }

    public PilhaDinamica(int tamanhoMaximo) {
        inicio = null;
        topo = null;
        qtdeItens = 0;
        MAX = tamanhoMaximo;
    }

    public boolean estaVazia() {
        return (inicio == null);
    }

    public boolean estaCheia() {
        if(MAX < 0) {
            return false;
        }
        return (qtdeItens == MAX);
    }

    public boolean empilhar(String dado) {
        if(estaCheia()) {
            return false;
        }
        No novoNo = new No(dado);
        if (estaVazia()) {
            inicio = novoNo;
        } else {
            topo.setProximo(novoNo);
        }
        topo = novoNo;
        qtdeItens++;
        return true;
    }

    public String desempilhar() {
        if(estaVazia()) {
            return "";
        }
        No aux = inicio;
        String dado = topo.getDado();
        qtdeItens--;

        if(inicio == topo) { // só tem 1 No na Pilha
            inicio = null;
            topo = null;
            return dado;
        }
        // percorre a lista até o penultimo No
        while(aux.getProximo() != topo) {
            aux = aux.getProximo(); // move o aux para o próximo No da lista
        }
        aux.setProximo(null); // finaliza a lista neste ponto
        topo = aux; // atualiza a referência do TOPO para o No anterior

        return dado;
    }

    public void exibir() {
        if (estaVazia()) {
            System.out.println("Pilha Vazia");
            return;
        }
        No aux = inicio;
        do {
            System.out.println(aux.getDado());
            aux = aux.getProximo(); // move o No auxiliar para o próximo No da Pilha
        } while (aux != null);
    }

    public void exibirRecursivo() {
        if(estaVazia()){
            System.out.println("Pilha Vazia");
            return;
        }
        exibirRecursivo(inicio);
    }

    private void exibirRecursivo(No aux) {
        if(aux == topo) {
            System.out.println(aux.getDado());
            return;
        }
        exibirRecursivo(aux.getProximo());
        System.out.println(aux.getDado());
    }
}
